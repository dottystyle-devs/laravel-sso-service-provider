@guest
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script>
    (function($) {
        var cookieName = '{{ $cookie_var }}';

        window.sso_autologin = function(img) {
            var $img = $(img);
            var loggedIn = Cookies.get(cookieName);

            // Clear the cookie, invoke the callbacks then refresh the page 
            if (loggedIn == 1) {
                Cookies.remove(cookieName, { path: '' });
                document.location.reload(true);
            }
        };

    })(jQuery);
    </script>
    <img src="{{ $src }}" onload="javascript:sso_autologin(this);" height="1" width="1" style="visibility:hidden" />
@endguest