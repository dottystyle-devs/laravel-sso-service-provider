<script>
    (function (document, options) {
        var img = document.createElement('img')
    
        img.height = 1;
        img.width = 1;
        img.style.display = 'none';
        img.src = options.syncUrl;
        
        img.addEventListener('error', function (e) {
            // Reload the page to force authentication if necessary.
            window.location.reload();
        });
    
        document.body.appendChild(img);
    })(document, @json(compact(['syncUrl', 'loginUrl'])));
</script>