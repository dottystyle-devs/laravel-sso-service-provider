<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Contracts;

use Dottystyle\LaravelSSO\UserInfo;
use Illuminate\Contracts\Auth\Authenticatable;

interface User extends Authenticatable
{
    /**
     * Fill the instance with details from the user info.
     * 
     * @param \Dottystyle\LaravelSSO\UserInfo $info
     * @return void
     */
    public function setUserInfo(UserInfo $info);

    /**
     * Get the corresponding user info id of the user.
     * 
     * @return mixed
     */
    public function getUserInfoId();
}