<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Contracts;

use Dottystyle\LaravelSSO\UserInfo;

interface UserProvider
{
    /**
     * Retrieve the corresponding user by the user info from SSO.
     * 
     * @param Dottystyle\LaravelSSO\UserInfo $user
     * @return \Dottystyle\LaravelSSO\ServiceProvider\Contracts\User
     */
    public function retrieveByUserInfo(UserInfo $user);
}