<?php

namespace Dottystyle\LaravelSSO\ServiceProvider;

use Illuminate\Support\Facades\Facade as BaseFacade;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;

class Facade extends BaseFacade
{
    const FACADE_OBJECT_NAME = 'sso.service_provider.manager';

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return self::FACADE_OBJECT_NAME;
    }

    /**
     * Register the login and logout routes of the application.
     * We will just redirect the login and logout to our SSO routes.
     * 
     * @param array $options (optional)
     * @param string $options[logout_redirect] Redirect after logging out. This must be a named route.
     * @return void
     */
    public static function routes(array $options = [])
    {
        $options += ['logout_redirect' => 'home'];
    
        // Register the default login and logout routes to redirect to SSO's
        Route::get('login', function () {
            return Redirect::route('sso.login', request()->query());
        })->name('login');

        Route::get('logout', function () use ($options) {            
            return Redirect::route('sso.logout', [
                'return_url' => $options['logout_redirect']
            ]);
        })->name('logout');
    }
}