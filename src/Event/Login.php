<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Event;

use Dottystyle\LaravelSSO\UserInfo;

class Login 
{
    /**
     * @param \Dottystyle\LaravelSSO\UserInfo $userInfo
     */
    protected $userInfo;

    /**
     * Create new instance of the event.
     * 
     * @param \Dottystyle\LaravelSSO\UserInfo $userInfo
     */
    public function __construct(UserInfo $user)
    {
        $this->userInfo = $userInfo;
    }

    /**
     * Get the info of the logged in user.
     * 
     * @return \Dottystyle\LaravelSSO\UserInfo
     */
    public function getUserInfo()
    {
        return $this->userInfo;
    }
}