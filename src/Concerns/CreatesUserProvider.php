<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Concerns;

use Dottystyle\LaravelSSO\Contracts\UserProvider;
use Dottystyle\LaravelSSO\ServiceProvider\EloquentUserProvider;
use InvalidArgumentException;
use Closure;

trait CreatesUserProvider
{
    /**
     * @var array
     */
    protected $customUserProviderCreators = [];

    /**
     * Create a user provider implementation.
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\BrokerUserProvider
     */
    protected function createUserProvider()
    {
        $config = $this->app['config']['sso.users.provider'];
        $driver = $config['driver'];
    
        if (isset($this->customUserProviderCreators[$driver])) {
            return call_user_func($this->customUserProviderCreators[$driver], $this->app, $config);
        }

        switch ($driver) {
            case 'eloquent': 
                return $this->createEloquentUserProvider($config);
            
            default:
                throw new InvalidArgumentException(
                    "SSO user provider [{$driver}] is not defined."
                );
        }
    }

    /**
     * Create an eloquent user provider.
     * 
     * @param array $config
     * @return \Dottystyle\LaravelSSO\ServiceProvider\EloquentUserProvider
     */
    protected function createEloquentUserProvider(array $config)
    {
        return new EloquentUserProvider($config['model'], $config['attribute'], $config['info_key']);
    }

    /**
     * Add a new user provider.
     * 
     * @param string $name
     * @param \Closure $callback
     * @return void
     */
    public function userProvider(string $name, Closure $callback)
    {
        $this->customUserProviderCreators[$name] = $callback;
    }
}