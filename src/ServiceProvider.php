<?php

namespace Dottystyle\LaravelSSO\ServiceProvider;

use Dottystyle\LaravelSSO\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     */
    protected $name; 

    /**
     * @var string
     */
    protected $secret;

    /**
     * Create new instance of service provider.
     * 
     * @param mixed $id
     * @param string $name
     * @param string $secret
     */
    public function __construct($id, $name, $secret)
    {
        $this->id = $id;
        $this->name = $name;
        $this->secret = $secret;
    }

    /**
     * Get the id of service provider.
     * 
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the name of service provider.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the secret key of service provider.
     * 
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }
}