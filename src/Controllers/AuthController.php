<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Controllers;

use Dottystyle\LaravelSSO\RequestUtils;
use Dottystyle\LaravelSSO\ServiceProvider\Manager;
use Dottystyle\LaravelSSO\Exceptions\LoginException;
use Dottystyle\LaravelSSO\Exceptions\SSOException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    /**
     * @var \Dottystyle\LaravelSSO\ServiceProvider\BrokerManager
     */
    protected $manager;

    /**
     * @param \Dottystyle\LaravelSSO\ServiceProvider\BrokerManager
     */
    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Redirect the user to the SSO login page.
     * 
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        return redirect($this->manager->getLoginUrl(
            $request->query('continue', url('/'))
        ));
    }

    /**
     * Receives and verifies login response from the SSO server.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function receiveLogin(Request $request)
    {
        $acceptsImage = RequestUtils::acceptsImage($request);

        try {
            if (! $request->input('success', false)) {
                return $this->receiveFailedLogin($request);
            }
            
            $this->manager->verifyLoginSuccess(
                $request->input('sig'), 
                $token = $request->input('token'), 
                $request->input('user_id'), 
                $request->input('nonce')
            );

            $this->manager->loginUsingToken($token, $request->input('expires_at'));
            
            if ($acceptsImage) {
                return $this->respondWithImage($request, true);
            } else {
                return redirect($request->query('continue'));
            }
        } catch (SSOException $e) {
            // Always return an image even if there has been an exception if content requested is image.
            if ($acceptsImage) {
                return $this->respondWithImage($request, false);
            }

            throw $e;
        }
    }

    /**
     * Process failed login received from identity provider.
     * 
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    protected function receiveFailedLogin(Request $request)
    {
        $this->manager->verifyLoginFail($request->input('sig'), $request->input('nonce'));

        throw new LoginException($request->input('message'), $request->input('code', 0));
    }

    /**
     * Respond with a 1x1 pixel image.
     * 
     * @param \Illuminate\Http\Request $request
     * @param bool $loggedIn
     * @return \Illuminate\Http\Response
     */
    protected function respondWithImage(Request $request, bool $loggedIn)
    {
        return RequestUtils::transparent1x1PngResponse()
            // Let the script access the flag
            ->cookie('sso_autologin_success', $loggedIn ? '1' : '0', 1, null, null, false, false);
    }

    /**
     * Logout the user (deletes token thereby invalidating current session).
     * 
     * @param string $redirect (optional)
     * @return void 
     */
    public function logout(Request $request)
    {
        return redirect($this->manager->getLogoutUrl($request->input('return_url')));
    }

    /**
     * Receive response from a token comparison request.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function receiveTokenCompare(Request $request)
    {
        $this->manager->verifyTokenCompare($request->input('sig'), $request->input('sp_token'), $request->input('salt'));
        $success = $request->input('success') == '1';
        $statusCode = $success ? 200 : 419;

        // Logout the user if we received a failed token comparison response as the token is not synced with the identity provider.
        if (! $success) {
            $this->manager->logout();
        }

        if (RequestUtils::acceptsImage($request)) {
            // We will return a blank response if the requests wants an image as the HTTP status is irrelevant as long as the
            // response payload is a valid image.
            return $success ? RequestUtils::transparent1x1PngResponse() : response()->setStatusCode($statusCode);
        }

        return response()->json(['result' => $success], $statusCode);
    }
}