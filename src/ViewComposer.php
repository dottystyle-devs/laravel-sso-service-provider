<?php

namespace Dottystyle\LaravelSSO\ServiceProvider;

use Illuminate\View\View;

class ViewComposer
{
    /**
     * @var \Dottystyle\LaravelSSO\ServiceProvider\Manager
     */
    protected $sso;

    /**
     * Create new instance of view composer.
     * 
     * @param \Dottystyle\LaravelSSO\ServiceProvider\Manager $sso
     */
    public function __construct(Manager $sso)
    {
        $this->sso = $sso;
    }

    /**
     * Bind data to the view.
     * 
     * @param \Illuminate\View\View $view
     * @return void
     */
    public function syncToken(View $view)
    {
        $view->syncUrl = $this->sso->getTokenCompareUrl();

        // Bind SSO's login route when login URL is not specified.
        if (! isset($view->loginUrl)) {
            $view->loginUrl = route('sso.login');
        }
    }
}