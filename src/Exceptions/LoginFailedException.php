<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Exceptions;

use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;

class LoginFailedException extends AuthenticationException
{
}