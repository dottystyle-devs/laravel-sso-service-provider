<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Exceptions;

use Dottystyle\LaravelSSO\Exceptions\SSOException;

class GetUserInfoException extends SSOException
{
}