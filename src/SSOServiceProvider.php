<?php

namespace Dottystyle\LaravelSSO\ServiceProvider;

use Dottystyle\LaravelSSO\Signature\HMACSignatureAssistant;
use Dottystyle\LaravelSSO\ServiceProvider\Contracts\UserProvider;
use Dottystyle\LaravelSSO\ServiceProvider\Middleware\Authenticate;
use Dottystyle\LaravelSSO\ServiceProvider\Listeners\LoginListener;
use Dottystyle\LaravelSSO\ServiceProvider\Controllers\AuthController;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class SSOServiceProvider extends IlluminateServiceProvider
{
    /**
     * Register application services.
     *
     * @return void
     */
    public function boot()
    {
        $root = dirname(__DIR__);

        $this->publishes([ "$root/config.php" => config_path('sso.php') ], 'config');
        $this->mergeConfigFrom("$root/config.php", 'sso');

        $this->loadRoutesFrom("$root/routes.php");
        $this->loadViewsFrom("$root/resources/views", 'sso');

        $this->publishes([
            "$root/resources/views" => resource_path('views/vendor/sso')
        ]);

        $this->registerViewComposer();
        $this->excludeTokensFromEncryption();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerManager();
        $this->registerMiddleware();
    }

    /**
     * Register the manager instance.
     * 
     * @return void
     */
    protected function registerManager()
    {
        $this->app->singleton('sso.service_provider.manager', function ($app) {
            $config = $app['config'];
            
            return new Manager($app, 
                $app['sso.service_provider'], 
                new HMACSignatureAssistant,
                $config['sso.idp'], 
                $config['sso.token']
            );
        });

        $this->app->singleton('sso.service_provider', function ($app) {
            $config = $app['config'];
            $class = $config['sso.service_provider.class'];

            if ($class === ServiceProvider::class) {
                return new ServiceProvider(
                    $config['sso.service_provider.id'], 
                    $config['sso.service_provider.name'], 
                    $config['sso.service_provider.secret']
                );
            } else {
                return new $class($config['sso.service_provider']);
            }
        });

        // Register the controller to handle the requests 
        $this->app->bind(AuthController::class, function () {
            return new AuthController($this->app['sso.service_provider.manager']);
        });

        $this->app->alias('sso.service_provider.manager', Manager::class);
    }

    /**
     * Register the broker middleware.
     * 
     * @return void
     */
    protected function registerMiddleware()
    {
        $this->app->bind(Authenticate::class, function ($app) {
            return new Authenticate($app['sso.service_provider.manager'], $app['auth']);
        });

        $this->app['router']->aliasMiddleware('sso.auth', Authenticate::class);
    }

    /**
     * Register view composer.
     * 
     * @return void
     */
    protected function registerViewComposer()
    {
        $this->app['view']->composer('sso::autologin', function($view) {
            $view->with([
                'src' => $this->app['sso.service_provider.manager']->getLoginUrl(url('/')),
                'cookie_var' => 'sso_autologin_success'
            ]);
        });

        $this->app['view']->composer('sso::check', ViewComposer::class.'@syncToken');
    }

    /**
     * Exclude the token cookie from being encrypted by the middleware.
     * 
     * @return void
     */
    protected function excludeTokensFromEncryption()
    {
        $this->app->resolving(EncryptCookies::class, function ($middleware) {
            $middleware->disableFor($this->app->config['sso.token']);
        });
    }

    /**
     * Get the list of provided services.
     * 
     * @return array
     */
    public function provides()
    {
        return ['sso.service_provider', 'sso.service_provider.manager', Manager::class];
    }
}