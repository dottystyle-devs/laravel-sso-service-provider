<?php

namespace Dottystyle\LaravelSSO\ServiceProvider\Middleware;

use Dottystyle\LaravelSSO\Exceptions\AuthenticationException;
use Dottystyle\LaravelSSO\Exceptions\MismatchedUsersException;
use Dottystyle\LaravelSSO\ServiceProvider\Manager;
use Dottystyle\LaravelSSO\ServiceProvider\Exceptions\GetUserInfoException;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException as IlluminateAuthenticationException;
use Closure;

class Authenticate
{
    /**
     * @var \Dottystyle\LaravelSSO\ServiceProvider\Manager
     */
    protected $manager;

    /**
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * @param \Dottystyle\LaravelSSO\ServiceProvider\Manager $manager
     * @param \Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Manager $manager, AuthFactory $auth)
    {
        $this->manager = $manager;
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string[] $guards
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {        
        try {
            // Get token from cookies. 
            $token = $request->cookies->get($this->manager->getTokenName());
            // Logout the user if we don't have any token
            if (! $token) {
                throw new AuthenticationException();
            }

            $this->manager->setToken($token);

            // Make sure the locally logged in user is the same with the user who owns the token if
            // the user is already logged in.
            if ($this->auth->check()) {
                $this->validateSameUser();
            } else {
                $this->manager->attemptLogin();
            }

            return $next($request);
        } catch (GetUserInfoException $e) {
            // We could not get the user info from our token for some reason.
            // TODO
            // Is this the correct action?
            $this->logout($request);
        } catch (AuthenticationException $e) {
            // Force logout even if the user is logged in since it is likely we have an invalid token
            $this->logout($request);
        }
    }

    /**
    * Log the user.
    * 
    * @param \Illuminate\Http\Request $request
    * @return void
    */
    protected function logout($request)
    {
        $this->manager->logout();
        // Invalidate current session.
        $request->session()->invalidate();

        throw new IlluminateAuthenticationException();
    }

    /**
     * Validates the auth user id is the same with the token's user id.
     * 
     * @return bool
     */
    protected function validateSameUser()
    {
        if (! $this->auth->user()->is($this->manager->getUser())) {
            throw new MismatchedUsersException();
        }
    }
}