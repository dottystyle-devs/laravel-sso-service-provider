<?php

namespace Dottystyle\LaravelSSO\ServiceProvider;

use Dottystyle\LaravelSSO\UserInfo;
use Illuminate\Database\Eloquent\Model;

class EloquentUserProvider implements Contracts\UserProvider
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @var string The attribute to match with the userinfo key.
     */
    protected $attribute;

    /**
     * @var string
     */
    protected $infoKey;

    /**
     * Create new instance of the user provider.
     * 
     * @param string $model
     * @param string $attribute (optional) 
     * @param string $key (optional)
     */
    public function __construct($model, $attribute = '', $key = '')
    {
        $this->model = ($model instanceof Model) ? $model : (new $model);
        $this->attribute = $attribute;
        $this->infoKey = $key;
    }

    /**
     * Save the user from SSO to our local user repository.
     * 
     * @param \Dottystyle\LaravelSSO\UserInfo $userInfo
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function retrieveByUserInfo(UserInfo $userInfo)
    {
        $modelClass = $this->model;
        $id = $this->infoKey ? $userInfo[$this->infoKey] : $userInfo->getId();

        if ($this->attribute) {
            $user = $this->model->newQuery()->where($this->attribute, '=', $id)->first();
        } else {
            $user = $this->model->newQuery()->find($id);
        }
        // Create a new record if not yet existing
        $user or ($user = $this->model->newInstance());
        // Save new user or update existing 
        $user->setUserInfo($userInfo);

        return $user;
    }
}