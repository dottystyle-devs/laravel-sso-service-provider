<?php

namespace Dottystyle\LaravelSSO\ServiceProvider;

use Dottystyle\LaravelSSO\UserInfo;
use Dottystyle\LaravelSSO\ServiceProviderInterface;
use Dottystyle\LaravelSSO\RequestUtils;
use Dottystyle\LaravelSSO\Signature\SignatureAssistant;
use Dottystyle\LaravelSSO\Signature\ServiceProviderSignatureAssistant;
use Dottystyle\LaravelSSO\Signature\ForwardsServiceProviderSignatureCalls;
use Dottystyle\LaravelSSO\Exceptions\SSOException;
use Dottystyle\LaravelSSO\Exceptions\InvalidSignatureException;
use Dottystyle\LaravelSSO\Exceptions\MissingTokenException;
use Dottystyle\LaravelSSO\ServiceProvider\Exceptions\GetUserInfoException;
use Dottystyle\LaravelSSO\ServiceProvider\Exceptions\GetTokenStatException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use GuzzleHttp\Exception\TransferException;

class Manager
{
    use ForwardsServiceProviderSignatureCalls,
        Concerns\SendsHttpRequests, 
        Concerns\CreatesUserProvider;

    /**
     * @var \Illuminate\Container\Container
     */
    protected $app;

    /**
     * @var \Dottystyle\LaravelSSO\ServiceProviderInterface 
     */
    protected $serviceProvider;

    /**
     * @var \Dottystye\LaravelSSO\Signature\ServiceProviderSignatureAssistant
     */
    protected $sig;

    /**
     * @var array
     */
    protected $idpOptions;

    /**
     * @var string 
     */
    protected $tokenName;

    /**
     * @var string The SSO token id received from the identity provider (or in cookie)
     */
    protected $token;

    /**
     * @var \Dottystyle\LaravelSSO\ServiceProvider\Contracts\User
     */
    protected $user;

    /**
     * Create new instance of the manager.
     * 
     * @param \Illuminate\Container\Container $app
     * @param \Dottystyle\LaravelSSO\ServiceProviderInterface $sp
     * @param \Dottystyle\LaravelSSO\Signature\SignatureAssistant $sig
     * @param array $idpOptions
     * @param string $tokenName
     */
    public function __construct($app, ServiceProviderInterface $sp, SignatureAssistant $sig, array $idpOptions, $tokenName) 
    {
        $this->app = $app;
        $this->tokenName = $tokenName;
        $this->serviceProvider = $sp;

        $this->idpOptions = array_merge($idpOptions, [
            'url' => rtrim($idpOptions['url'], '/')
        ]);

        $this->users = $this->createUserProvider();
        $this->sig = (new ServiceProviderSignatureAssistant($sig))->setServiceProvider($sp);
    }

    /**
     * Get the service provider
     * 
     * @return \Dottystyle\LaravelSSO\ServiceProviderInterface
     */
    public function getServiceProvider() 
    {
        return $this->serviceProvider;
    }

    /**
     * Get the signature assistant for service provider.
     * 
     * @return \Dottystyle\LaravelSSO\Signature\ServiceProviderSignatureAssistant
     */
    public function getSignatureAssistant()
    {
        return $this->sig;
    }

    /**
     * Set the SSO token.
     * 
     * @param string $token
     * @return void
     */
    public function setToken($token) 
    {
        $this->token = $token;
    }

    /**
     * Get the name for the token.
     * 
     * @return string
     */
    public function getTokenName()
    {
        return $this->tokenName;
    }

    /**
     * Ensures token is set.
     * 
     * @return void
     * 
     * @throws \Dottystyle\LaravelSSO\Exceptions\MissingTokenException
     */
    protected function ensureToken() 
    {
        if (! $this->token) {
            throw new MissingTokenException();
        }
    }

    /**
     * Get the user provider instance.
     * 
     * @return \Dottystyle\LaravelSSO\ServiceProvider\Contracts\UserProvider
     */
    public function getUserProvider()
    {
        return $this->users;
    }

    /**
     * Get path relative to the identity provider base SSO URL.
     * 
     * @param string $path
     * @return string
     */
    protected function getIdpUrl($path, array $params = [])
    {
        $path = $this->idpOptions['url']."/sso/$path";

        return $params ? "$path?".http_build_query($params) : $path; 
    }

    /**
     * Create and send an HTTP request to server.
     * 
     * @param string $method
     * @param string $url
     * @param array $options (optional)
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    public function sendRequest($method, $url, array $options = []) {
        // Add the SSO token for every request sent to the server.
        if ($this->token) {
            $options['cookies'] = $options['cookies'] ?? []; 
            $options['cookies'][$this->idpOptions['token']] = $this->token;
        }

        $options['headers'] = $options['headers'] ?? [];
        $options['headers']['X-SSO-SP'] = $this->serviceProvider->getId();

        return $this->getHttpClient()->send(
            $this->getHttpClient()->createRequest($method, $url, $options)
        );
    }

    /**
     * Get the options when creating http client instance.
     * 
     * @return array
     */
    protected function getHttpClientOptions()
    {
        return [
            'headers' => ['Content-Type' => 'application/json']
        ];
    }

    /**
     * Get URL to attach session at SSO server.
     * 
     * @param string $continue
     * @return string
     */
    public function getLoginUrl($continue)
    {
        $returnUrl = $this->app['url']->route('sso.receive_login', compact('continue'));

        return $this->getIdpUrl('login', [
            'sp' => $this->serviceProvider->getId(),
            'nonce' => $nonce = $this->randomString(),
            'sig' => $this->signLogin($nonce, $returnUrl),
            'return_url' => $returnUrl
        ]);
    }

    /**
     * Generate random string
     * 
     * @param int $length (optional)
     * @return string
     */
    protected function randomString($length = 20)
    {
        return Str::random($length);
    }

    /**
     * Login the user using the token.
     * 
     * @param string $token
     * @param string $expiry
     * @return mixed
     * 
     * @throws \Dottystyle\LaravelSSO\ServiceProvider\SSOException
     */
    public function loginUsingToken($token, $expiry)
    {   
        $this->setToken($token);
        
        // Fetch the user data from SSO using the freshly received token
        $user = $this->getUser();

        // Create and set the cookie for the token.
        // The expiration (timestamp) of the cookie is sent together with the token to 
        // compute the remaining minutes of the token relative to broker's time.
        $expiresAt = Carbon::createFromTimestamp($expiry);
        
        // Set the token cookie
        $this->app['cookie']->queue(
            $this->app['cookie']->make(
                $this->tokenName, $token, max($expiresAt->diffInMinutes(Carbon::now()), 0)
            )
        );

        // Login using the default authentication guard
        $this->app['auth']->login($user);
    }

    /**
     * Attempt to login the user using the current token.
     * 
     * @param string $token
     * @return 
     */
    public function attemptLogin()
    {
        $this->ensureToken();

        $stat = $this->getTokenStat();

        $this->loginUsingToken($this->token, $stat['expires_at']);
    }

    /**
     * Get the logged in user.
     * 
     * @return \Dottystyle\LaravelSSO\ServiceProvider\UserInfo
     * 
     * @throws \Dottystyle\LaravelSSO\ServiceProvider\Exceptions\GetUserInfoException
     */
    public function getUserInfo()
    {
        try {
            return new UserInfo($this->getTokenStat()['user']);
        } catch (GetTokenStatException $e) {
            throw new GetUserInfoException();
        }
    }

    /**
     * Get the status of the current token (with user details).
     * 
     * @return object
     * 
     * @throws \Dottystyle\LaravelSSO\ServiceProvider\Exceptions\GetTokenStatException
     */
    public function getTokenStat()
    {
        try {
            $this->ensureToken();

            $response = $this->sendRequest('GET', $this->getIdpUrl('token'), [
                'query' => [
                    'salt' => $salt = $this->randomString(),
                    'sig' => $this->signTokenStat($salt)
                ]
            ]);
            
            if ($response->getStatusCode() !== 200) {
                throw new GetTokenStatException();
            }

            $result = $response->json();

            // Verify signature of the response 
            $this->verifyTokenStatSuccess($result['sig'] ?? '', $this->token, $salt);

            return $result['data'];
        } catch (TransferException $e) {
            throw new GetTokenStatException();
        }
    }

    /**
     * Get the URL to compare currently held token to identity provider's.
     * 
     * @return string
     */
    public function getTokenCompareUrl()
    {
        $this->ensureToken();

        return $this->getIdpUrl('compare', [
            'sp' => $this->serviceProvider->getId(),
            'sp_token' => $this->token,
            'salt' => $salt = RequestUtils::salt(),
            'sig' => $this->signTokenCompare($this->token, $salt),
            'success' => $successRedirectUrl ?? $this->app['url']->route('sso.receive_token_compare', ['success' => true]),
            'failed' => $failedRedirectUrl ?? $this->app['url']->route('sso.receive_token_compare', ['success' => false])
        ]);
    }

    /**
     * Get the logged in user.
     * 
     * @return \Dottystyle\LaravelSSO\ServiceProvider\Contracts\User
     */
    public function getUser()
    {
        if (! $this->user) {
            $this->user = $this->users->retrieveByUserInfo($this->getUserInfo());
        }

        return $this->user;
    }

    /**
     * Logout the user.
     * 
     * @return void
     */
    public function logout()
    {
        $this->app['auth']->logout();

        $this->app['cookie']->queue($this->app['cookie']->forget($this->getTokenName()));

        $this->token = null;
    }

    /**
     * Get the logout URL of the identity provider.
     * 
     * @param string $returnUrl
     * @return void
     */
    public function getLogoutUrl($returnUrl)
    {
        return $this->getIdpUrl('logout', [
            'return_url' => $returnUrl,
            'token' => $this->token,
            'sig' => $this->signLogout($this->token, $this->getUser()->getUserInfoId())
        ]);
    }

    /**
     * Forward some calls to the signature assistant.
     * 
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, array $arguments)
    {
        $result = $this->forwardToSignature($this->sig, $method, $arguments, $called);

        if (! $called) {
            throw new BadMethodCallException(sprintf(
                'Call to undefined method %s::%s', get_class($this), $method
            ));
        }

        return $result;
    }
}