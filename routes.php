<?php

Route::prefix('sso')
    ->namespace('Dottystyle\LaravelSSO\ServiceProvider\Controllers')
    ->middleware(['web'])
    ->group(function() {

        Route::get('login', 'AuthController@login')->name('sso.login');
        Route::get('login_cb', 'AuthController@receiveLogin')->name('sso.receive_login');
        Route::get('logout', 'AuthController@logout')->name('sso.logout');
        Route::get('compare_cb', 'AuthController@receiveTokenCompare')->name('sso.receive_token_compare');
        
    });