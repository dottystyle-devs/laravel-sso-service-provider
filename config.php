<?php

return [
   'service_provider' => [
        /**
         * @var string
         */
        'class' => Dottystyle\LaravelSSO\ServiceProvider\ServiceProvider::class,
        /**
         * @var string 
         */
        'id' => env('SSO_SERVICE_PROVIDER_ID'),
        /**
         * @var string
         */
        'name' => env('SSO_SERVICE_PROVIDER_NAME'),
        /**
         * @var string
         */
        'secret' => env('SSO_SERVICE_PROVIDER_SECRET'),
    ],
    /**
     * @var string
     */
    'idp' => [
        /**
         * @var string
         */
        'url' => env('SSO_IDENTITY_PROVIDER_URL'),
        /**
         * @var string 
         * 
         * The name of the token when connecting to the identity provider.
         * Should match the token name used by the identity provider.
         */
        'token' => env('SSO_IDENTITY_PROVIDER_TOKEN', 'sso_token')
    ],
    /**
     * @var string Route to redirect when after a successful login.
     */
    'login_success' => '',
    /**
     * @var string The name of the SSO token
     */
    'token' => env('SSO_TOKEN', 'sso_token'),
    /**
     * Pretty much like AuthUserProvider to retrieve the local user given the SSO user info.
     */
    'users' => [
        'provider' => [
            'driver' => 'eloquent',
            'model' => '',
            'attribute' => '',
            'info_key' => ''
        ]
    ]
];